Hi there,

Thank you for your message. We will get back to you shortly.

Cheers,
Your Service Desk team

---

For reference, here is the information we have collected:

- ISSUE_ID: %{ISSUE_ID}
- ISSUE_PATH: %{ISSUE_PATH}
- UNSUBSCRIBE_URL: %{UNSUBSCRIBE_URL}
- SYSTEM_HEADER: %{SYSTEM_HEADER}
- SYSTEM_FOOTER: %{SYSTEM_FOOTER}
- ADDITIONAL_TEXT: %{ADDITIONAL_TEXT}
- ISSUE_DESCRIPTION:

%{ISSUE_DESCRIPTION}

---
